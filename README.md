# \<author-card\>

An author card for a blog

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Using the Element
Call with `<author-card></author-card>`, available parameters are:

* `name` **(String)** - The name...
* `bio` **(String)** - Small text string for a bio snippet
* `email` **(String)** *(Optional)* - Used for generating the gravatar image
* `theme` **(String)** *(Optional)* - Available options are gray, blue
* `link` **(String)** - Where the "Read more" link goes to

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.
